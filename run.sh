#!/bin/bash

set -e

./run/consul
./run/registrator
./run/ceph
./run/mongo
./run/playlister
./run/streamer
./run/balancer
./run/pusher

