
Bootstrap scripts
-----------------

Scripts from this repo are used to bootstrap openott platform to single host quickly.

They are divided into two parts: init scripts to prepare non-disposable data and run scripts.
Thoose scripts are located in respective subfolders.

For bootstrap non production images are used like `dev` openott components (respective tag for docker images) and
non production `ceph/demo` docker image.

Prerequisites
-------------

You need linux or OS X with docker at least 1.7 installed and working on deploy host.

Configuration and State
-----------------------

Ceph uses configuration files which should be shared accross all components connected to ceph.
We store those configuration files in separate docker data container `cephconf`.

Ceph data volumes are not extracted in separate data container, because of some problems with ceph/demo.
So the data is stored in ceph container itself and in case of it upgrading all data will be lost.

Mongo data are persistent and separated to `mongodata` container. The mongo container itself is disposable.

So, the following *containers* holds state of openott:

- cephconf
- ceph
- mongodata

Some containers needs additional information like ip addr they need to advertise. This is configrable in `./init/defs.sh`.
You have to set ip addr of the host in this file along with `CEPH_NETWORK` parameter: cidr of the host ip.

Use `init/defs.sh.example` as an example of `init/defs.sh`.

Preparing
---------

Configure `./init/defs.sh` as said above.

One click launch
----------------

To do so you can use [bootstrap.sh](./bootstrap.sh) script. In this case you do not need any information below except adding content.

If you wish to take control over the process and do everything yourself, follow next steps.

Initialize
----------

Prepare data volumes and configure ceph.

```bash
./init/ceph
./init/mongodata
./init/segments
```

Now you can launch components. Every component (container) launched after this point is disposable in the following sence: you can discard, remove and relaunch this components without data loss.

Launching
---------

Run components in following sequence.

```
./run/consul
./run/registrator
./run/ceph
./run/mongo
./run/playlister
./run/streamer
./run/balancer
./run/pusher
```

OpenOTT is ready for storing content and streaming.

Add VOD content
---------------

Assume you have transcoded movie in several qualities (720p, 1080p) with good GOP structure (2 secs).

Add movie by segmenting it. Adjust `./run/segmenter` script using variables
`DIR`, `FILES`, `RESOURCE_ID` to your paths and files and run it.

You should see output like

```
2015.07.31 16:27:34.415 INFO [segmenter      ] built mts #1: duration 2.00 time: 20000000
2015.07.31 16:27:34.463 INFO [segmenter      ] built mts #2: duration 2.00 time: 40000000
2015.07.31 16:27:34.523 INFO [segmenter      ] built mts #3: duration 2.00 time: 60000000
2015.07.31 16:27:34.578 INFO [segmenter      ] built mts #4: duration 2.00 time: 80000000
2015.07.31 16:27:34.634 INFO [segmenter      ] built mts #5: duration 2.00 time: 100000000
```

and corresponding output at pusher logs: `docker logs pusher`

Now you can watch this movie using the any supported format by constructing necessary url, for smooth streaming it would be (substitute ip addr and resource_id):
```
http://<host>:8080/v0/pl/ss/standard/none/<resource-id>.ism/manifest?manifest_type=vod
```

and same movie as smooth live:
```
http://<host>:8080/v0/pl/ss/standard/none/<resource-id>.isml/manifest?manifest_type=live&start_from_null=1
```


Add LIVE content
----------------

To add live containt you need input mpegts multicast streams with regular gop structure by 2 secs and aligned to each other.
For ffmpeg use `-g 50` option for 25 FPS and transcode different qualities using single ffmpeg command (multiple outputs).

Assume you have such aligned multicasts at mcast group 239.0.0.1 on ports 1300-1302 for sd, hd, fhd qualities respectevely.
Feed it to segmenter using the following command

```
docker run --rm -it --volumes-from segments openott/segmenter:dev -i udp://239.0.0.1:1300 -i udp://239.0.0.1:1301 -i udp://239.0.0.1:1302 -r <resourceid> /segs/
```

It segments provided multicasts, store segments at `segments` data container where pusher watches for them and sends them to the data store.

Construct urls like in VOD case.
