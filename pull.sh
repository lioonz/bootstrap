#!/bin/bash

set -e

docker pull busybox
docker pull ceph/demo
docker pull gliderlabs/registrator
docker pull mongo:2
docker pull progrium/consul

docker pull openott/playlister:dev
docker pull openott/pusher:dev
docker pull openott/streamer:dev
docker pull openott/segmenter:dev
docker pull openott/balancer:dev
